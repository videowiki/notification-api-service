
const Notification = require('./models').Notification;

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
        Notification.count(req.query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })


    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }

        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        console.log(rest)
        if (one) {
            q = Notification.findOne(rest);
        } else {
            q = Notification.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((notifications) => {
            return res.json(notifications);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        Notification.create(data)
            .then((notification) => {
                return res.json(notification);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        console.log(req.body)
        Notification.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => Notification.find(conditions))
            .then(notifications => {
                return res.json(notifications);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let notifications;
        Notification.find(conditions)
            .then((a) => {
                notifications = a;
                return Notification.remove(conditions)
            })
            .then(() => {
                return res.json(notifications);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        Notification.findById(req.params.id)
            .then((notification) => {
                return res.json(notification);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        Notification.findByIdAndUpdate(id, { $set: changes })
            .then(() => Notification.findById(id))
            .then(notification => {
                return res.json(notification);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedNotification;
        Notification.findById(id)
            .then(notification => {
                deletedNotification = notification;
                return Notification.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedNotification);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    return router;
}